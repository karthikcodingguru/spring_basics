package com.java.traiing.car;

import javax.validation.constraints.NotNull;

public class Car {

	@NotNull(message = "number cannot be null")
	int carnumber;

	String carmodel;

	String cartype;

	int caryear;

	public Car(int carnumber, String carmodel, String cartype, int caryear) {

		this.carnumber = carnumber;
		this.carmodel = carmodel;
		this.cartype = cartype;
		this.caryear = caryear;
	}

	public Car() {
		// TODO Auto-generated constructor stub
	}

	public int getCarnumber() {
		return carnumber;
	}

	public void setCarnumber(int carnumber) {
		this.carnumber = carnumber;
	}

	public String getCarmodel() {
		return carmodel;
	}

	public void setCarmodel(String carmodel) {
		this.carmodel = carmodel;
	}

	public String getCartype() {
		return cartype;
	}

	public void setCartype(String cartype) {
		this.cartype = cartype;
	}

	public int getCaryear() {
		return caryear;
	}

	public void setCaryear(int caryear) {
		this.caryear = caryear;
	}

	@Override
	public String toString() {

		StringBuilder str = new StringBuilder();
		str.append(carnumber);
		str.append(carmodel);
		str.append(cartype);
		str.append(caryear);

		return str.toString();
	}

}
