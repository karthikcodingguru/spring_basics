package com.java.training.carweb;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.java.traiing.car.Car;
import com.java.training.carservelt.Carservice;

@Controller
public class CarController {

	@Autowired
	private Carservice service;

	@RequestMapping("/cars")
	public ModelAndView getCars() {

		List<Car> carlist = service.getallcars();

		ModelAndView mvc = new ModelAndView();

		mvc.addObject("carlist", carlist);
		mvc.setViewName("Carlist");

		return mvc;

	}

	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public ModelAndView createCarForm(Model model) {

		return new ModelAndView("Addcar", "car", new Car());

	}

	@RequestMapping(value = "/new", method = RequestMethod.POST)
	public String saveCar(@Valid Car car, BindingResult bindingresult) {

		if (bindingresult.hasErrors()) {

			return "Addcar";
		} else {

			service.create(car);
			return "ack";

		}

	}

}
