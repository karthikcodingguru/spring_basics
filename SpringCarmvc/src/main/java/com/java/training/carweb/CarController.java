package com.java.training.carweb;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.java.traiing.car.Car;
import com.java.training.carservelt.Carservice;

@Controller
public class CarController {

	@Autowired
	private Carservice service;

	@RequestMapping("/cars")
	public ModelAndView getCars() {

		List<Car> carlist = service.getallcars();

		ModelAndView mvc = new ModelAndView();

		mvc.addObject("carlist", carlist);
		mvc.setViewName("Carlist");

		return mvc;

	}

	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public ModelAndView createCarForm() {

		ModelAndView mvc = new ModelAndView();

		// Car car = new Car();
		// car.setCarmodel("Please enter the model");
		// car.setCartype("please enter the type");

		// mvc.addObject(car);
		mvc.addObject(new Car());
		// mvc.addObject("car", new Car());

		mvc.setViewName("Addcar");

		return mvc;

	}

	@RequestMapping(value = "/new", method = RequestMethod.POST)
	public String saveCar(Car car) {

		service.create(car);
		return "ack";
	}

}
