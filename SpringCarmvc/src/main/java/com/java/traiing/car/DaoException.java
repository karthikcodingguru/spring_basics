package com.java.traiing.car;

public class DaoException extends RuntimeException {
	
	
	public DaoException(Throwable ex){
		super (ex);
	}
	
	public DaoException(String errormsg, Throwable ex){
		super (errormsg,ex);
	}

}
