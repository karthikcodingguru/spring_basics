package com.java.training.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.java.traiing.car.Car;
import com.java.training.carservelt.Carservice;
import com.java.training.carservelt.Carserviceimpl;

/**
 * Servlet implementation class Carlistservlet
 */
public class Carlistservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public Carlistservlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		Carservice service = new Carserviceimpl();
		List <Car> carlist = service.getallcars();
		
		request.setAttribute("carsdisplay", carlist);
	
		
		RequestDispatcher dispacher = request.getRequestDispatcher("/WEB-INF/jsp/jstlpage.jsp");
		
		dispacher.forward(request,response);
		
	}
}