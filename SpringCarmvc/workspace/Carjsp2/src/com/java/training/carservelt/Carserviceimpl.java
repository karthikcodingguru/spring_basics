package com.java.training.carservelt;

import java.util.List;

import com.java.traiing.car.Car;
import com.java.traiing.car.CarDao;
import com.java.traiing.car.CarDaoImpl;

public class Carserviceimpl implements Carservice {

	public void create(Car car) {

		CarDao dao = new CarDaoImpl();
		dao.createCar(car);
	}

	public List<Car> getallcars() {

		CarDao dao = new CarDaoImpl();
		
		return dao.getallCar();
	}

}
