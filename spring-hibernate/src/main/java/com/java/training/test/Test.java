package com.java.training.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.java.training.dao.HibernteImpls;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ApplicationContext ctx = new ClassPathXmlApplicationContext(
				"Springconfig.xml");
		HibernteImpls dao = ctx.getBean("hibernteImpls", HibernteImpls.class);

		System.out.println(dao.getCircleCount());

	}

}
