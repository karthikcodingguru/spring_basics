package com.java.training.maven.SpringBasics;

public class Truck implements Vehicle {

	@Override
	public void start() {
		System.out.println("truck is starting");
	}

	@Override
	public void drive() {
		System.out.println("truck is driving");
	}

	@Override
	public void stop() {
		System.out.println("truck is stopping");
	}

}
