package com.java.training.maven.SpringBasics;

public interface Vehicle {

	public void start();

	public void drive();

	public void stop();

}
