package com.java.training.maven.SpringBasics;

public class Car implements Vehicle {

	@Override
	public void start() {

		System.out.println("Car has started");
	}

	@Override
	public void drive() {

		System.out.println("Car is driving");
	}

	@Override
	public void stop() {

		System.out.println("Car has stopped");
	}

}
