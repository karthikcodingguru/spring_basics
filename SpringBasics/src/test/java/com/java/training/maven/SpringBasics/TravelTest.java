package com.java.training.maven.SpringBasics;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TravelTest {

	@Test
	public void test() {

		ApplicationContext context = new ClassPathXmlApplicationContext(
				"spring-config.xml");

		Traveller traveller = (Traveller) context.getBean("traveller");

		traveller.travel();

	}

}
