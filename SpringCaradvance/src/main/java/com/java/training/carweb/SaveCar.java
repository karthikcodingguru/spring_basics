package com.java.training.carweb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.java.traiing.car.Car;
import com.java.training.carservelt.Carservice;

public class SaveCar extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		CarForm carForm = (CarForm) form;
		// specific servlet
		WebApplicationContext context = WebApplicationContextUtils
				.getWebApplicationContext(servlet.getServletContext());

		Carservice service = (Carservice) context.getBean(Carservice.class);

		Car car = new Car(carForm.getCarnumber(), carForm.getCarmodel(),
				carForm.getCartype(), carForm.getCaryear());

		service.create(car);

		return mapping.findForward("ack");
	}

}
