package com.java.training.carweb;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.java.traiing.car.Car;
import com.java.training.carservelt.Carservice;

public class ListCar extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		BeanFactory factory = WebApplicationContextUtils
				.getWebApplicationContext(servlet.getServletContext());

		Carservice service = (Carservice) factory.getBean(Carservice.class);

		List<Car> carlist = service.getallcars();

		service.getallcars();

		request.setAttribute("carsdisplay", carlist);

		request.getRequestDispatcher("/WEB-INF/JSP/Carlist.jsp");

		return mapping.findForward("list");

	}
}
