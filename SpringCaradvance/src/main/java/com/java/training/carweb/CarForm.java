package com.java.training.carweb;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class CarForm extends ActionForm {

	private int carnumber;
	private String carmodel;
	private String cartype;
	private int caryear;

	public int getCarnumber() {
		return carnumber;
	}

	public void setCarnumber(int carnumber) {
		this.carnumber = carnumber;
	}

	public String getCarmodel() {
		return carmodel;
	}

	public void setCarmodel(String carmodel) {
		this.carmodel = carmodel;
	}

	public String getCartype() {
		return cartype;
	}

	public void setCartype(String cartype) {
		this.cartype = cartype;
	}

	public int getCaryear() {
		return caryear;
	}

	public void setCaryear(int caryear) {
		this.caryear = caryear;
	}

	@Override
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		String action = request.getParameter("action");

		if (action != null && action.equals("save")) {

			if (carmodel == null || carmodel.length() == 0) {

				errors.add(carmodel, new ActionMessage("carmodel.required"));

			}
			if (cartype == null || cartype.length() == 0) {

				errors.add(cartype, new ActionMessage("cartype.required"));

			}
		}

		return errors;
	}
}
