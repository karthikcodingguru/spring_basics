package com.java.training.carservelt;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.traiing.car.Car;
import com.java.traiing.car.CarDao;

@Service
public class Carserviceimpl implements Carservice {

	@Autowired
	private CarDao dao;

	@Override
	public void create(Car car) {

		dao.createCar(car);
	}

	// listing only toyota cars
	@Override
	public List<Car> getallcars() {

		List<Car> allcars = dao.getallCar();

		List<Car> mahendracars = new ArrayList<Car>();

		for (Car acar : allcars) {

			if (acar.getCarmodel() != null
					&& acar.getCarmodel().equalsIgnoreCase("Toyota")) {

				mahendracars.add(acar);
			}
		}

		return mahendracars;
	}

}
