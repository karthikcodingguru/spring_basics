package com.java.cars;

public interface Vehicle {

	public void driving();

	public void stop();

}
