package com.java.cars;

public class Car implements Vehicle {

	@Override
	public void driving() {
		System.out.println("Car Driving");
	}

	@Override
	public void stop() {
		System.out.println("Car Stopped");
	}

}
