package com.java.cars;

public class Truck implements Vehicle {

	@Override
	public void driving() {
		System.out.println(" Truck driving");
	}

	@Override
	public void stop() {
		System.out.println("Truck Stopped");
	}

}
