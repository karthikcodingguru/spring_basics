package com.training.java.carweb;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.java.traiing.car.Car;
import com.java.training.carservelt.Carservice;

public class ListCar extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ApplicationContext context = new ClassPathXmlApplicationContext(
				"spring-config.xml");

		Carservice service = (Carservice) context.getBean("cardaojdbc");

		List<Car> carlist = service.getallcars();

		service.getallcars();

		request.setAttribute("carsdisplay", carlist);

		request.getRequestDispatcher("/WEB-INF/JSP/Carlist.jsp");

		return mapping.findForward("list");

	}

}
