package com.java.traiing.car;

import java.util.ArrayList;
import java.util.List;

public class CarDaoDummyImpl implements CarDao {

	@Override
	public List<Car> getallCar() {

		List<Car> dcarlist = new ArrayList<Car>();

		Car car1 = new Car(15, "TATA", "SEDAN", 2012);
		Car car2 = new Car(16, "MAHENDRA", "HASHBACK", 2011);
		Car car3 = new Car(17, "Toyota", "sedan", 2011);
		Car car4 = new Car(11, "toyota", "coope", 2011);
		dcarlist.add(car1);
		dcarlist.add(car2);
		dcarlist.add(car3);
		dcarlist.add(car4);

		return dcarlist;
	}

	@Override
	public void createCar(Car car) {
		// TODO Auto-generated method stub

	}

}
