package com.java.training.carservelt;

import java.util.ArrayList;
import java.util.List;

import com.java.traiing.car.Car;
import com.java.traiing.car.CarDao;

public class Carserviceimpl implements Carservice {

	private CarDao dao;

	public Carserviceimpl(CarDao dao) {

		this.dao = dao;

	}

	@Override
	public void create(Car car) {

		dao.createCar(car);
	}

	@Override
	public List<Car> getallcars() {

		List<Car> allcars = dao.getallCar();

		List<Car> mahendracars = new ArrayList<Car>();

		for (Car acar : allcars) {

			if (acar.getCarmodel() != null
					&& acar.getCarmodel().equalsIgnoreCase("Toyota")) {

				mahendracars.add(acar);
			}
		}

		return mahendracars;
	}

}
