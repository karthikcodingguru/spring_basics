package com.java.training.service.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.java.traiing.car.Car;
import com.java.training.carservelt.Carservice;

public class CarServiceTest {

	@Test
	public void serviceTest() {

		ApplicationContext context = new ClassPathXmlApplicationContext(
				"spring-config.xml");

		Carservice service = (Carservice) context.getBean(Carservice.class);

		List<Car> carlist = service.getallcars();
		assertEquals(2, carlist.size());

		for (Car acar : carlist) {

			System.out.println(acar.getCarmodel());
			System.out.println(acar.getCarnumber());
			System.out.println(acar.getCartype());
			System.out.println(acar.getCaryear());
		}
	}

}
